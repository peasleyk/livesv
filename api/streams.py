from .base import API


class Streams(API):
    def __init__(self, key):
        super().__init__(key)

    def game(self, game, limit):
        """
        Search for top streamers on a game, return a list of length limit live streamers
        """
        game = game.replace(" ", "%20")
        url = f"/streams?game={game}&limit={limit}"
        return self.makeReq(url)

    def channel(self, name):
        """
        Takes in the name of a streamer, returns an object with information on the channel
        """
        ID = self.getIdfromName(name)
        url = f"/streams/{ID}"
        return self.makeReq(url)

    def followed(self):
        url = "/streams/followed"
        return self.makeReq(url)