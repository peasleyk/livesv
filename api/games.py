from .base import API


class Games(API):
    def __init__(self, key):
        super().__init__(key)

    def top(self, limit):
        """
        Search for a top streamers on a game, return a list of length limit live streamers
        """
        url = f"/games/top?limit={limit}"
        return self.makeReq(url)
