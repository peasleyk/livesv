from api.search import Search
from api.streams import Streams
from api.channels import Channels
from api.games import Games


class Client(object):
    def __init__(self, key):
        self.key = key
        self._Channels = None
        self._Streams = None
        self._Search = None
        self._Games = None

    @property
    def Channels(self):
        if not self._Channels:
            self._Channels = Channels(self.key)

        return self._Channels

    @property
    def Streams(self):
        if not self._Streams:
            self._Streams = Streams(self.key)

        return self._Streams

    @property
    def Search(self):
        if not self._Search:
            self._Search = Search(self.key)

        return self._Search

    @property
    def Games(self):
        if not self._Games:
            self._Games = Games(self.key)

        return self._Games
