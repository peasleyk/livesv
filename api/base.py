import logging
import urllib.request
import urllib.error
import json
from types import SimpleNamespace as Namespace


class API:
    def __init__(self, key):
        self.key = key
        self.log = logging.getLogger("livesv")

    def makeReq(self, url):
        """
        Calls to twitchs API, and returns am object from the json result
        """
        url = "https://api.twitch.tv/kraken" + url
        data = urllib.request.Request(url)
        data.add_header("Client-id", self.key)
        data.add_header("Accept", "application/vnd.twitchtv.v5+json")

        try:
            data = urllib.request.urlopen(data)
        except urllib.error.HTTPError as e:
            self.log.error(f"Problem getting response with url {url}: {e}")
            return False

        # This was added after a really weird error when searching for omega
        except UnicodeError as e:
            self.log.error(
                f"Unicode Error with processing request for {data}, giving error {e}"
            )
            return False

        data = data.read().decode("utf-8")
        # TODO - Make class to create object instead of this hack
        rObject = json.loads(data, object_hook=lambda d: Namespace(**d))
        return rObject

    def getIdfromName(self, streamer):
        """
        Given a streamer return that users twitch ID
        """
        url = "/users?login={}".format(streamer)
        rObject = self.makeReq(url)
        if rObject.users:
            return rObject.users[0]._id
        return None
