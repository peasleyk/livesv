from .base import API


class Channels(API):
    def __init__(self, key):
        super().__init__(key)

    def videos(self, name, numVids):
        """
        Returns past broadcasts of a streamer
        """
        ID = self.getIdfromName(name)
        url = f"/channels/{ID}/videos?broadcasts=true&broadcast_type=archive&limit={numVids}"
        return self.makeReq(url)

    def info(self, name):
        """
        Returns past broadcasts of a streamer
        """
        ID = self.getIdfromName(name)
        url = f"/channels/{ID}/"
        return self.makeReq(url)
