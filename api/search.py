from .base import API


class Search(API):
    def __init__(self, key):
        super().__init__(key)

    @staticmethod
    def mostcommon(gamesList):
        """
        Returns the most common element in a list
        """
        return max(set(gamesList), key=gamesList.count)

    def gameName(self, game, limit):
        """
        Utility function that takes in a fuzzy game search (cs:go, lol, wow) and tries to match it to a valid game.
        Useful for a user game search.
        """
        gamesList = []
        game = game.replace(" ", "%20")
        url = f"/search/streams?query={game}&limit={limit}"
        rObject = self.makeReq(url)
        if not rObject:
            return False

        for stream in rObject.streams:
            gamesList.append(stream.channel.game)

        if len(gamesList) == 0:
            return False

        finalGame = self.mostcommon(gamesList)
        return finalGame
