#!/usr/bin/python3
import sys
import os
import argparse
import logging
import subprocess
import shutil
import inquirer
import yaml

import api
import iclient

from datetime import datetime
from multiprocessing import Pool

# Needed to be able to call this script from outside the dir it"s in
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

# Import our configurations
config = yaml.safe_load(open(f"{ROOT_DIR}/config.yml"))

# Global client as we're not using classes
client = api.Client(config["key"])

# Set up logging
logger = logging.getLogger("livesv")
logging.basicConfig(format="%(levelname)s: %(message)s", level=config["level"])

PREFFERED_PLAYER = config["player"]
QUALITY = config["quality"]
STREAMERS = config["streamers"]
TWITCH_URL = "https://twitch.tv/"


def getTimeDifference(vodTime):
    """
    Returns the difference between now and a vidTime in days
    """

    currentTime = datetime.now()
    extractedTime = datetime.strptime(vodTime, "%Y-%m-%dT%H:%M:%SZ")

    return (currentTime - extractedTime).days


def interactiveSearch(game, limit):
    """
    Will continually search for inputs until a streamer (number) is chosen
    Prints out the top limit streamers
    """

    def redo(message=None):
        """
        Utility to restart this function on error
        """
        if message:
            logging.info(message)
        choice = ""
        while choice == "":
            choice = input("Game: ")
            interactiveSearch(choice, limit)

    foundGame = client.Search.gameName(game, limit)
    if not foundGame:
        redo("Game not found, try again")

    streamers = client.Streams.game(foundGame, limit)
    if not streamers:
        redo("Twitch returned a null result for some reason, try a different search")

    print(f"\nGame: {foundGame}\n")

    if len(streamers.streams) < 1:
        redo("No streamers found for this game")
    else:
        max_name = len(
            max(streamers.streams, key=lambda x: len(x.channel.name)).channel.name
        )
        max_viewers = len(str(max(streamers.streams, key=lambda x: x.viewers).viewers))
        displayList = []
        streamerList = []
        for streamer in streamers.streams:
            streamerList.append(streamer.channel.name)
            displayList.append(
                f"[{streamer.viewers:{max_viewers}}] {streamer.channel.name:{max_name}}: {streamer.channel.status}"
            )

        streamerList.append(None)
        displayList.append("Search for another game")
        to_choose = list(zip(displayList, streamerList))

        selection = inqDisplay("Game to search", to_choose)
        # Handles interrupt
        if not selection:
            return

    streamer = selection["choice"]
    if not streamer:
        redo("")
    else:
        playVideo(streamer)


def getTopGames(limit):
    """
    Gets top games from twitch
    """
    result = client.Games.top(limit)
    games, viewers = [], []
    for game in result.top:
        games.append(game.game.name)
        viewers.append(game.viewers)

    max_game = len(max(games, key=len))
    displayList = []
    for x in range(0, len(games)):
        displayList.append(f"({x}) {games[x]:{max_game}} {viewers[x]:,}")
    choices = list(zip(displayList, games))
    game = inqDisplay("Game to search", choices)

    if game:
        interactiveSearch(game["choice"], limit)


def playVideo(url, vod=False):
    """
    Launches a video
    """
    if not vod:
        url = TWITCH_URL + url

    # subprocess.call(["python", "-m", "streamlink", url, QUALITY, "--player", PREFFERED_PLAYER])
    # logging.info("Launching firefox")
    subprocess.call(["firefox", url])


def inqDisplay(inqMessage, choices):
    """
    Displays an inquirer list
    """
    questions = [
        inquirer.List("choice", message=inqMessage, choices=choices, carousel=True),
    ]
    choice = inquirer.prompt(questions)
    return choice


def listVods(streamer, limit):
    """
    List vods for a streamer
    """
    videos = client.Channels.videos(streamer, limit)
    if videos._total == 0:
        logging.error("No past broadcasts found")
        return

    width = shutil.get_terminal_size().columns
    vodlist, displayList = [], []
    lengthLimit = 20
    for video in videos.videos:
        vodlist.append(video.url)
        diff = getTimeDifference(video.created_at)
        vodLine = f"Age: {diff} Days, Title: {video.title.strip()} Game: {video.game}"
        if len(vodLine) > width - lengthLimit:
            vodLine = vodLine[: width - lengthLimit] + "..."
        displayList.append(vodLine)

    choices = list(zip(displayList, vodlist))
    vod = inqDisplay("Vod to Play", choices)
    if vod:
        playVideo(vod["choice"], vod=True)


def getCurrentStream(streamer):
    """
    Searches for a streamer and returns a formatted line
    to pint out, as well as their on-line status
    """
    channel = client.Streams.channel(streamer)
    online = True
    if channel and channel.stream:
        game = channel.stream.game
        viewers = channel.stream.viewers
        displayLine = f"{streamer}: {game} - {viewers}"
        return [online, displayLine]

    online = False
    displayLine = f"{streamer}: Show VOD list"
    return [online, displayLine]


def listStreamers(streamers, limit):
    """
    Loops through each name in streamers and finds if they are live, and how many viewers they have
    If they are not live, the latest vod will be played if available.
    """

    padding = len(max(streamers, key=len))
    streamers = [f"{x:{padding}}" for x in streamers]
    pool = Pool(len(streamers))
    results = pool.map(getCurrentStream, streamers)
    pool.close()
    pool.join()

    statusList, displayList = zip(*results)
    choices = list(zip(displayList, streamers))
    streamer = inqDisplay("Streamer to play", choices)
    if streamer:
        online = statusList[streamers.index(streamer["choice"])]
        if online:
            playVideo(streamer["choice"])
            return

        listVods(streamer["choice"], limit)


def info(channel):
    """
    Get info on a channel
    """
    streamer = client.Channels.info(channel)

    if not streamer:
        logging.info(f"Channel {channel} not found")
        return

    keep_keys = [
        "name",
        "display_name",
        "status",
        "mature",
        "broadcaster_software",
        "game",
        "created_at",
        "partner",
        "views",
        "followers",
        "description",
    ]
    max_key = len(max(keep_keys, key=lambda x: len(x)))

    for key in keep_keys:
        formated_value = f"{streamer.__dict__[key]}"
        if str(streamer.__dict__[key]).isdigit():
            formated_value = f"{streamer.__dict__[key]:,}"

        print(f"{key:{max_key}}: {formated_value}")


def irc(username, channel, oauth):
    irc = iclient.IRC_Client(username, channel, oauth)
    for name, message in irc.chat_gen():
        print(f"{name:55}: {message}")


def main(argv):
    logging.debug("Entering DEBUG")
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-v",
        "--view",
        help="View supplied (hard coded) streamers status",
        action="store_true",
    )
    parser.add_argument(
        "-p", "--play", action="store_true", help="Play a stream or view vods"
    )
    parser.add_argument(
        "-i", "--irc", action="store_true", help="Stream a channels chat"
    )
    parser.add_argument("-t", "--top", action="store_true", help="search top games")
    parser.add_argument(
        "-dd",
        "--deep-dive",
        action="store_true",
        help="Stats on a streamer",
    )
    parser.add_argument("-s", "--search", type=str, help="Search for a game")
    parser.add_argument(
        "-c",
        "--channel",
        type=str,
        help="Streamer to use for above options",
    )
    parser.add_argument(
        "-l",
        "--limit",
        type=int,
        default=10,
        help="limit when getting results (games, streamers, vods)",
    )

    parser.parse_args()
    args = parser.parse_args()

    if True in [args.deep_dive, args.irc, args.play]:
        if not args.channel:
            logging.info("-c or --channel required")
            return

    if args.deep_dive:
        info(args.channel)
    elif args.irc:
        irc(config.get("username"), args.channel, config.get("oauth"))
    elif args.play:
        response = client.Streams.channel(args.channel)
        if not response:
            logging.error(f"Could not find streamer '{args.channel}'")
        if response.stream:
            playVideo(args.channel)
        else:
            listVods(args.channel, args.limit)
    elif args.search:
        interactiveSearch(args.search, args.limit)
    elif args.top:
        getTopGames(args.limit)
    elif args.view:
        if not STREAMERS:
            logging.error("No streamers entered in config")
            return
        listStreamers(STREAMERS, args.limit)
    else:
        parser.print_help()


if __name__ == "__main__":
    try:
        main(sys.argv[1:])
    except KeyboardInterrupt:
        sys.exit()
