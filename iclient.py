import select
import socket
import random
from datetime import datetime


class IRC_Client:
    def __init__(self, username, streamer, oauth):
        self.username = username
        self.streamer = streamer
        self.oauth = oauth
        self.url = "irc.chat.twitch.tv"
        self.port = 6667
        self.last_rand = 0
        self.sock = None

    @staticmethod
    def timestamp(name):
        """
        Adds a timestamp to a name
        """
        now = datetime.now().strftime("%Y-%m-%d %H:%M")
        return f"[{now}] {name}"

    def _add_color(self, name):
        """
        Gets a random non repeating color to format
        a user name with
        """
        bold = "\u001b[1m"
        highlight = "\u001b[7m"
        end = "\033[0m"
        colors = {
            "red": "\u001b[31;1m",
            "green": "\u001b[32;1m",
            "yellow": "\u001b[33;1m",
            "blue": "\u001b[34;1m",
            "purple": "\u001b[35;1m",
            "cyan": "\u001b[36;1m",
            "white": "\u001b[37;1m",
        }
        # If we choose the same value, wrap around
        choice = random.randint(0, len(colors) - 1)
        if choice == self.last_rand:
            choice = (choice + 1) % len(colors)
        self.last_rand = choice
        rand_color = list(colors.keys())[choice]

        new_name = f"{bold}{colors[rand_color]}{name}{end}"
        if name == self.streamer:
            new_name = f"{highlight}{new_name}{end}"

        return new_name

    def _init_room(self):
        """
        Connects a socket to the defined chat room
        """
        sock = socket.create_connection((self.url, self.port))
        sock.send(f"PASS oauth:{self.oauth}\r\n".encode("utf-8"))
        sock.send(f"NICK {self.username}\r\n".encode("utf-8"))
        sock.send(f"JOIN #{self.streamer}\r\n".encode("utf-8"))
        self.sock = sock

    def chat_gen(self):
        """
        Connects to a channels chat using IRC
        and displays it
        """
        # TODO - better parsing
        pong = "PONG :tmi.twitch.tv\r\n".encode("utf-8")
        self._init_room()
        while True:
            # Blocks the socket until it receives data'
            #
            ready = select.select([self.sock], [], [])
            if ready[0]:
                response = self.sock.recv(2 ** 14).decode("utf-8", "ignore")
                # More than one message came come through on the wire,
                # and they are split by new lines. The buffer is large so we can
                # Shove more message though
                # Need to filter out any zero length messages and process them all
                responses = response.split("\n")
                responses = list(filter(lambda x: len(x) > 0, responses))
                for r in responses:
                    if "PING" in r:
                        self.sock.send(pong)
                        continue
                    else:
                        name = r.split("!")[0].strip(":")
                        name = self.timestamp(self._add_color(name))
                        message = r.split(":")[-1].strip("\n")
                        if len(message) > 200:
                            continue

                    yield (name, message)
