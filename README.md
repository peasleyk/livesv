# What
A twitch client to browse streams, view chats, and see who's online and other stats. A combination of functions I use to play around and watch twitch.

# Configuration:

## Config file

All configuration is in the config.yml file
A twitch Client-Id is needed, get one from your account settings

See config.yml.def for a sample configuration - remove ".def" and replace the API key, as well as enter some streamer usernames

## Dependencies

To install dependencies, run

```pipenv install --system``` to install packages to system,
```pipenv install``` to install to virtualenv


# Features And Usage

Usage

```shell
» livesv --help
usage: livesv.py [-h] [-v] [-p] [-i] [-t] [-dd] [-s SEARCH] [-c CHANNEL] [-l LIMIT]

optional arguments:
  -h, --help            show this help message and exit
  -v, --view            View supplied (hard coded) streamers status
  -p, --play            Play a stream or view vods
  -i, --irc             Stream a channels chat
  -t, --top             search top games
  -dd, --deep-dive      Stats on a streamer
  -s SEARCH, --search SEARCH
                        Search for a game
  -c CHANNEL, --channel CHANNEL
                        Streamer to use for above options
  -l LIMIT, --limit LIMIT
                        limit when getting results (games, streamers, vods)
```

You can:
- List status of defined streamers
- View top games
- Search for games
- Play vods
- Get stats on a streamer

## List Saved Streamer Statuses
After entering a list of streamers int he config, you can select one to stream
```shell
» livesv --view
INFO: Querying twitch...
[?] Streamer to play: northernlion     : Play last VOD
 > northernlion     : Play last VOD
   rockleesmile     : Play last VOD
   sinvicta         : Slay the Spire - 113
   sleepcycles      : Play last VOD
   baertaffy        : Play last VOD

```

## Play a Streamer Directly

You can play a stream directly
```shell
> livesv --play --channel ziggyDLive
[cli][info] Found matching plugin twitch for URL https://twitch.tv/ziggyDLive
[cli][info] Available streams: audio_only, 160p (worst), 360p, 480p, 720p (best)
[cli][info] Opening stream: 720p (hls)
[cli][info] Starting player: mpv
```

## Playing Vods

If a streamer is offline, you can choose a VOD to play from either ```--view``` or ```--play```, with the amount shown defined by ```--limit```
Select the vod using the arrow keys and enter.

```shell
» livesv --play --channel northernlion
[?] Vod to Play: Age: 0 Days, Title: it's dark souls baby Game: Dark Souls II
 > Age: 0 Days, Title: it's dark souls baby Game: Dark Souls II
   Age: 1 Days, Title: The Northernlion Live Super Show! (Gunfire Reborn / Clank / Skrib) Game: Gunfire Reborn
   Age: 2 Days, Title: unity then baseball Game: Call of Duty: Modern Warfare
   Age: 2 Days, Title: unity then baseball Game: Call of Duty: Modern Warfare
   Age: 2 Days, Title: The Northernlion Live Super Show! (The Golf Club / Tower Unite / Clubhouse) Game: The Golf Club 2019 Featuring PGA Tour
   Age: 4 Days, Title: i'll see you at the clubhouse (so you won't be lonely) Game: Clubhouse Games: 51 Worldwide Classics
   Age: 7 Days, Title: The Northernlion Live Super Show!  Game: Unfortunate Spacemen
   Age: 8 Days, Title: Curse of the Dead Gods New Update - !curse - #ad Game: Curse of the Dead Gods
   Age: 9 Days, Title: it's tuesday (unity) Game: Call of Duty: Modern Warfare
   Age: 9 Days, Title: The Northernlion Live Super Show! (Golf Club 2019 / Ticket to Ride / Codenames) Game: The Golf Club 2019 Featuring PGA Tour
```

## Searching For a Game

Also included it a game browser, allowing you to search for games and play streams.
Select the stream using the arrow keys and enter, or select "Search..." to enter a new query.


```
» livesv -s among

Game: Among Us

[?] Game to search: Search for another game
   [100562] xqcow        : MASTERMIND TOURNAMENT WARLORD AND WORLD RECORD HOLDER SMASHES EVERYONE TODAY. DONT MISS OUT. CLICK OR PERRISH.
   [ 35470] gaules       : AMONG US DA TRIBO - @Gaules nas Redes Sociais !premio !prime !Sorteio PC Gamer
   [ 27436] trainwreckstv: AMONG US ALL DAY + 10K TOURNEY AT 2PM PST| !twitter | !podcast
   [ 22812] ludwig       : $10,000 AMONG US TOURNAMENT
   [ 11019] 5uppp        : Among us tournament | !tournament !rules
   [ 10382] skeppylive   : Among Us with BadBoyHalo and Others!
   [  6407] boomtv       : Among Us Code Red $10,000 Event! | Lobby 2 | Ft. Trainwrecks, Soulja Boy, JuJu, Adept, Sliker, Punz, Ludwig, SypherPK, 5Up & Nakat! 
   [  6361] chilledchaos : IMPOSTER CHAOS! (Among Us w/ Friends)
   [  5538] kyr_sp33dy   : Among Us! Phasmophobia later then Marbles!
   [  5012] punz         : $10,000 AMONG US TOURNAMENT LULW | @Punztw on Twitter
 > Search for another game


```

## View Top Games
You can search for the top <x> games, then select one to go into interactive search

```shell
» livesv --top
[?] Game to search: (0) League of Legends            392,285
 > (0) League of Legends            392,285
   (1) Just Chatting                243,100
   (2) Among Us                     151,435
   (3) Fortnite                     138,153
   (4) Call of Duty: Modern Warfare 108,519
   (5) Grand Theft Auto V           84,132
   (6) Music                        61,617
   (7) Fall Guys                    57,471
   (8) DayZ                         52,399
   (9) Apex Legends                 43,227
```

## View a channels stats
Check views, followers, creation date, and a few more public things
```shell
» livesv --deep-dive --channel shroud
name                : shroud
display_name        : shroud
status              : DayZ Survival Simulator  | Follow @shroud on socials
mature              : False
broadcaster_software: unknown_rtmp
game                : DayZ
created_at          : 2012-11-03T15:50:32Z
partner             : True
views               : 384,766,875
followers           : 8,206,035
description         : I'm back baby
```